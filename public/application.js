/*
 * Temporary data
 * can i get the author of each build without navigating to each?
 */

var dummyData = [
  {
  number: 19651,
  url: "http://jenkins.vaddio.com/jenkins/job/yocto-parameterized-build/19651/",
  author: "Terry"
  },
  {
  number: 19650,
  url: "http://jenkins.vaddio.com/jenkins/job/yocto-parameterized-build/19650/",
  author: "Bjorn"
  },
  {
  number: 19649,
  url: "http://jenkins.vaddio.com/jenkins/job/yocto-parameterized-build/19649/"
  author: "Aaron"
  },
  {
  number: 19648,
  url: "http://jenkins.vaddio.com/jenkins/job/yocto-parameterized-build/19648/"
  },
  {
  number: 19647,
  url: "http://jenkins.vaddio.com/jenkins/job/yocto-parameterized-build/19647/"
  },
];

var JenkinsApp = {};
/*
 * Main app
 */
var MainApplication = Backbone.Marionette.Application.extend({
  setRootLayout: function() {
    this.root = new JenkinsApp.RootLayout();
  }
});

JenkinsApp.App = new MainApplication();

JenkinsApp.App.on('before:start', function() {
    JenkinsApp.App.setRootLayout();
});

JenkinsApp.App.on('start', function() {
  Backbone.history.start();
});

/*
 * Root layout
 */
JenkinsApp.RootLayout = Backbone.Marionette.LayoutView.extend({
  el: '#app',

  regions: {
    header: '#header',
    sidebar: '#sidebar',
    filters: '#filters'
  }
});


/*
 * Models
 */
JenkinsApp.Build = Backbone.Model.extend({
  defaults: {
    author: 'vaddio',
  }
});


/*
 * Collections
 */
var BuildCollection = Backbone.Collection.extend({
  model: JenkinsApp.Build,
});

JenkinsApp.BuildCollection = new BuildCollection(dummyData);


/*
 * Views
 */
JenkinsApp.BuildView = Backbone.Marionette.View.extend({
  tagName: 
});

JenkinsApp.BuildsListView = Backbone.Marionette.CompositeView.extend({
  template:
});

/*
 * Start main app
 */
JenkinsApp.App.start();
